$(document).ready(function(){

  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
      var cookies = document.cookie.split(';');
      for (var i = 0; i < cookies.length; i++) {
        var cookie = jQuery.trim(cookies[i]);
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) === (name + '=')) {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }
  //coba cari tahu, mengapa kita perlu mengambil csrftoken dari cookie
  var csrftoken = getCookie('csrftoken');
  //token yang ada di variable csrftoken kalian masukan dipassing ke dalam fungsi xhr.setRequestHeader("X-CSRFToken", csrftoken);
  //coba cari tahu, kenapa kita perlu mensetup csrf token terlebih dahulu sebelum melakukan request post ke views django

  //yang disarankan dari dokumentasi django nya
  function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }

  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
      if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken);
      }
    }
  });
  
  $("#submitID").fadeTo(0, 0.2);
  $("#submitID").prop("disabled", true);
  
  $("#alerts").children().hide();  
  
  //setup before functions
  var typing_timer;                //timer identifier
  var typingTime = 200;
  var $input = $(":input");
  
  //on keyup, start the countdown
  $input.on('keyup', function () {
    clearTimeout(typing_timer);
    typing_timer = setTimeout(typingPaused, typingTime);
  });
  
  //on keydown, clear the countdown
  $input.on('keydown', function () {
    clearTimeout(typing_timer);
  });
  
  function typingPaused () {
    $.ajax({
      method : "POST",
      url : "emailCheck",
      data : {email:$("#id_email").val()},
      dataType : "json",
      success : function (datajson) {
        var terdaftar = datajson.email_is_taken;
        console.log(terdaftar);
        var cek_nama = $("#id_nama").val().length > 0;
        var cek_pass = $("#id_password").val().length > 0;
        var valid = cek_nama && cek_pass;
        function cek_email() {
			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return $("#id_email").val().length > 0 && re.test($("#id_email").val());
		}
        if (terdaftar) {
          $("#emailExist").slideDown();
        } else {
          $("#emailExist").slideUp();
        }
		if (valid && !terdaftar) {
			if(cek_email()) {
				$("#submitID").fadeTo(600, 1);
				$("#emailFormatWrong").slideUp();
				$("#submitID").prop("disabled", false);
			}else {
				$("#emailFormatWrong").slideDown();
				$("#submitID").prop("disabled", true);
			}
		} else {
		  $("#submitID").fadeTo(600, 0.2);
		  $("#submitID").prop("disabled", true);
		}

      },
	  
	  error: function (error) {
        console.log("Data JSON tidak berhasil diakses");
      },
    });
  };

  $("#submitID").click(function(){
    $.ajax({
      method : "POST",
      data : $('form').serialize(),
      success : function (datajson) {
        $("#regSuccess").slideDown();
        $("#submitID").fadeTo(0, 0.2);
        $("#submitID").prop("disabled", true);
        window.setTimeout(function() {
            $("#regSuccess").slideUp();
        }, 3000);
		$("#id_nama").html("");
		$("#id_password").html("");
		$("#id_email").html("");
      },
    });
  });

  $.ajax({
    url : "regis_json",
    dataType : "json",
    success : function (datajson) {
      var dataSubs = datajson.subscribers;
      for (var i = 0; i < dataSubs.length; i++) {
        var info = dataSubs[i];
        var nama = info.nama;
        var email = info.email;
        var email_id = email.replace(/[^\w\s!?]/g,'');
        var baristabel = '<tr id = "baris' + email_id + '">' +
          "<td align='center'>" +
            '<button type="button" class="btn btn-danger unsub" style="background-color:rgb(110, 44, 0);padding:0.1vmax 0.6vmax" email_id = "' + email + '">' +
            '<span style=font-size:0.7vmax;> X </span>' +
            '</button>' +
          "</td>" +
          "<th scope=\"row\">" + nama + "</th>" +
          "<td>" + email + "</td>" +
        "</tr>";
        $('#fillThePlayerIn').append(baristabel);
      };
    },
	error: function (error) {
      var baristabel = "<tr><td colspan=\"3\" class = \"text-center\">JSON gagal di-load :(</td></tr>";
      $('#fillThePlayerIn').append(baristabel);
    },
  });

  $(document).on('click', '.unsub', function(){
    var email = $(this).attr('email_id');
    var idbersih = email.replace(/[^\w\s!?]/g,'');
    $.ajax({
      method : "POST",
      url : "delReg",
      data : {'email':email},
      dataType : "json",
    });
    $("#baris" + idbersih).remove();
  });
  
});
