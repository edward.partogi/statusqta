from django import forms
from .models import Status_Model, regis_Model

class Status_Form(forms.ModelForm):
    class Meta:
        model = Status_Model
        fields = "__all__"
        attributs = {"class" : "form-control",
             "placeholder" : "What's on your mind?"}
        widgets = {
            "statusModel" : forms.TextInput(attrs = attributs)
            }

class passwordWidget(forms.PasswordInput):
    input_type = 'password'

class regis_Form(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(regis_Form, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
        
    class Meta:
        model = regis_Model
        fields = [
            'nama',
            'email',
            'password',
        ]
        widgets = {
                'password' : passwordWidget(),
            }
