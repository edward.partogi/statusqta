from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .forms import Status_Form, regis_Form
from .models import Status_Model, regis_Model

# Create your views here.

def status(request):
    response = {}
    formStat = Status_Form(request.POST or None)
    if(request.method == 'POST' and formStat.is_valid()):
        formStat.save()
    stat = Status_Model.objects.all()
    response['formStat'] = formStat
    response['stat'] = stat
    return render(request, 'statusQmi.html', response)

@csrf_exempt
def regis(request):
    if(request.method == 'POST'):
        form = regis_Form(request.POST)
        print(form)
        if(form.is_valid()):
            nama = request.POST['nama']
            email = request.POST['email']
            password = request.POST['password']
            form = regis_Model(nama=nama, email=email, password=password)
            if not regis_Model.objects.filter(email = email).exists():
                form.save()
            return redirect('/regis_json')
    else:
        form = regis_Form()
        return render(request, 'regis.html', {'form':form})

def regis_json(request):
    regisDict = [obj.as_dict() for obj in regis_Model.objects.all()]
    return JsonResponse({"subscribers": regisDict}, content_type='application/json')

def cek_email(request):
    emailExist = False
    if 'email' in request.POST:
        emailExist = regis_Model.objects.filter(email = request.POST['email']).exists()
    return JsonResponse({"email_is_taken": emailExist}, content_type='application/json')

def hapus_reg(request):
    email = request.POST.get('email', '')
    query = regis_Model.objects.filter(email=email)
    if query:
        query.get().delete()
    return redirect('/regis')
