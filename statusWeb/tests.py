from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views as v
from . import models as m

import time

# Create your tests here.

class Homepage_testcase(TestCase):

    def test_status_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_using_status_func(self):
        found = resolve('/')
        self.assertEqual(found.func, v.status)

    def test_regis_url_is_exist(self):
        response = Client().get('/regis')
        self.assertEqual(response.status_code, 200)

    def test_regis_func(self):
        found = resolve('/regis')
        self.assertEqual(found.func, v.regis)

    def test_regisjson_url_is_exist(self):
        response = Client().get('/regis_json')
        self.assertEqual(response.status_code, 200)

    def test_regisjson_func(self):
        found = resolve('/regis_json')
        self.assertEqual(found.func, v.regis_json)

    def test_delreg_url_is_exist(self):
        response = Client().get('/delReg')
        self.assertEqual(response.status_code, 302)

    def test_delreg_func(self):
        found = resolve('/delReg')
        self.assertEqual(found.func, v.hapus_reg)

    def test_cekemail_url_is_exist(self):
        response = Client().get('/emailCheck')
        self.assertEqual(response.status_code, 200)

    def test_cekemail_func(self):
        found = resolve('/emailCheck')
        self.assertEqual(found.func, v.cek_email)

    def test_modelsub(self):
        nama = 'Kak Pewe'
        email = 'kakpewe@pepew.com'
        password = '123456'
        m.regis_Model.objects.create(nama = nama, email = email, password = password)
        count = m.regis_Model.objects.all().count()
        self.assertEqual(1, count)
        obj = m.regis_Model.objects.all()[:1].get()
        expected = {'nama': nama, 'email': email}
        self.assertEqual(expected, obj.as_dict())

    def test_submit_form(self):
        nama = 'Kak Pewe'
        email = 'kakpewe@pepew.com'
        password = '123456'
        response_post = self.client.post('/regis', {'nama': nama, 'email': email, 'password': password})
        self.assertEqual(response_post.status_code, 302)

        # response_post = Client().post('/registration/', {'nama': test, 'password': '16532000'})

        # lanjutkan lagi
