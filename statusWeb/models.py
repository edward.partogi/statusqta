from django.db import models

class Status_Model(models.Model):
    statusModel = models.CharField(max_length=300)
    created_date = models.DateTimeField(auto_now_add=True)

class regis_Model(models.Model):
    nama = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    password = models.CharField(max_length=100)

    def as_dict(self):
        return {
            "nama": self.nama,
            "email": self.email,
        }
