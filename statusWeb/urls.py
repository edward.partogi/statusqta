from django.urls import path
from .views import *

urlpatterns = [
    path('', status, name='status'),
    path('regis', regis, name='regis'),
    path('regis_json', regis_json, name='regisInJson'),
    path('emailCheck', cek_email, name='emailCheck'),
    path('delReg', hapus_reg, name='deleteReg')
]
